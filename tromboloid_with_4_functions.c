//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>

float input1()
{
   float a;
   printf("enter the height of tromboloid= ");
   scanf("%f",&a);
   return a;
}

float input2()
{
    float a;
    printf("enter the depth of tromboloid=");
    scanf("%f",&a);
    return a;
}
float input3()
{
    float a;
    printf("enter the breadth of tromboloid=");
    scanf("%f",&a);
    return a;
}
float calculate(float h,float b,float d)
{
    float v;
    v=((h*d)+d)/(3*b);
    return(v);
}
float output(float v)
{
    printf("\nvolume of tromboloid=%0.2f",v);
}
int main()
{
    float h1,b1,d1,v1;
    h1=input1();
    d1=input2();
    b1=input3();
    v1=calculate(h1,b1,d1);
    output(v1);
    return 0;
}

