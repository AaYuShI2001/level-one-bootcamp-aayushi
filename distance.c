//WAP to find the distance between two point using 4 function
#include <stdio.h>
#include <math.h>
float firstprint()
{
    printf("enter the abscissa and ordinate of point1 and then point 2 respectively");
}
float input()
{
    float a;
    scanf("%f",&a);
    return a;
}
float calc(float x1,float y1,float x2,float y2)
{
    float i1,i2,d;
    i1=pow((x1-x2),2);
    i2=pow((y1-y2),2);
    d=sqrt((i1+i2));
    return d;
}
float output(float d)
{
    printf("distance between the 2 points is: %.2f",d);
}
int main()
{
    float x1,x2,y1,y2,d;
    firstprint();
    x1=input();
    y1=input();
    x2=input();
    y2=input();
    d=calc(x1,y1,x2,y2);
    output(d);
}