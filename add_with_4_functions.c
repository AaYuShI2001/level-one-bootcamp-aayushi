//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>

float input()
{
   float a;
   printf("\nenter the no.");
   scanf("%f",&a);
   return a;
}
float calculate(float a,float b)
{
    float s;
    s=a+b;
    return(s);
}
float output(float s)
{
    printf("\nsum of the nos.=%0.2f",s);
}
int main()
{
    float num1,num2,s1;
    num1=input();
    num2=input();
    s1=calculate(num1,num2);
    output(s1);
    return 0;
}
